Demo of Emergo with Gamebrics.

**DISCLAIMER**

This is a proof-of-concept. Run at your own risk and only on a local machine. NEVER expose any ports to a network or to the internet! You can enter and edit data in the application, but assume these will not be persisted.

- install docker (tested with 24.0.6) and docker-compose (1.29.2), or, in Windows, Docker Desktop for Windows (tested with version 4.29.0), which contains both
- clone this docker compose repository to a directory of your choice
- clone the repository with the binaries (`https://gitlab.com/ou-teli/gamebrics-binaries`) to a temporary directory of your choice, *outside* the directory with the docker compose repository, e.g `cd /tmp . git clone https://gitlab.com/ou-teli/gamebrics-binaries.git` and move the directories `blob/` and `streaming/` to the directory with the docker compose repository
- in the docker compose repository open a terminal, and start with `docker compose up`
- point your browser to `http://localhost:8081/emergo`
- to investigate the game as game developer, log in with user `Gamebrics_author` password `Author_12345!`
	* to examine the Gamebrics components and their configuration and content, click on the `Gamebrics author` button (**UNDER CONSTRUCTION**)
	* alternatively, you can preview the game in the Emergo player, by successively clicking on the `case components` button, on the `content` button at the `game navigation` component, on `root` text, and finally on the `preview` item in the menu pop up
- to play the game, there are 2 accounts: you can log in with user id `Student1` or id `Student2`, both with password `Student_12345!`


